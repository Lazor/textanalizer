﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;//для буфера считывания/записи
using System.Text.RegularExpressions;// для Regex

namespace TextAnalizer
{
    public partial class TextAnalizer : Form
    {
        Dictionary<int, double> uniqWordsStatistics = new Dictionary<int, double>();//для графика

        bool mouseDown = false;

        public TextAnalizer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// получение адреса файла, из которого производить чтение текста
        /// </summary>
        private void btOpen_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();//диалоговое окно открытия файла
            string newFileAddress = openFileDialog.FileName;//получение выбранного имени файла

            lbAdress.Text = newFileAddress;
            
            Graphics gr = panelUniqWordsStatisticGraphic.CreateGraphics();
            gr.Clear(panelUniqWordsStatisticGraphic.BackColor);
            uniqWordsStatistics.Clear();
            rtbWords.Text = "";
            rtbPopularWords.Text = "";
            rtbOrigins.Text = "";
            lbAllChars.Text = "...";
            lbWordChars.Text = "...";
            lbParagraphs.Text = "...";
            lbAllWords.Text = "...";
            lbUniqueWords.Text = "...";
            lbAvergWordsLen.Text = "...";
            lbAvergWordsInParagraph.Text = "...";
            lbAvergCharsInParagraph.Text = "...";
            progressBar.Value = 0;
        }

        private void btAnaliza_Click(object sender, EventArgs e)
        {
            progressBar.Value = 0;
            double allCharsCount, allWordCharsCount, paragraphsCount=1, allWordsCount, uniqWordsCount;

            FileStream fstr = new FileStream(lbAdress.Text, FileMode.Open, FileAccess.Read);

            StreamReader streamRd = new StreamReader(fstr,Encoding.UTF32);

            string bufText = streamRd.ReadToEnd();

            if (bufText == null || bufText == "")
            {
                MessageBox.Show("Файл пуст");
                return;
            }

            bufText = bufText.ToLower();

            lbAllChars.Text = "Всего символов  -  "+bufText.Length;
            allCharsCount = bufText.Length;
            lbAllChars.Update();

            for (int i = (int)allCharsCount - 1; i >= 0; i--)
            {
                char s = bufText[i];
                if (s == '.' || s == ',' || s == ';' || s == ':' || s == '?' || s == '!' || s == '"' || s == '‘'
                    || s == '≪' || s == '≫' || s == '«' || s == '»' || s == '…' || s == '(' || s == ')'
                    || s == '–' || s == '*' || s == '“' || s == '„')
                {
                    bufText = bufText.Remove(i, 1);
                }

                else if (s == '\n' || s == '\t')
                {
                    bufText = bufText.Substring(0, i) + " " + bufText.Substring(i + 1);
                    paragraphsCount++;
                }
                else if (s == '\t')
                {
                    bufText = bufText.Substring(0, i) + " " + bufText.Substring(i + 1);
                }

                    progressBar.Value = (int)(progressBar.Maximum*0.6 * (allCharsCount-i) / allCharsCount);
            }

            lbParagraphs.Text = "Абзацев  -  " + paragraphsCount;
            lbParagraphs.Update();

            allWordCharsCount = bufText.Length;
            lbWordChars.Text = "Символов в словах  -  " + bufText.Length
                + " ("+Math.Round(100.00* allWordCharsCount / allCharsCount, 2) +"%)";
            lbWordChars.Update();

            List<string> originWords = new List<string>();
            originWords = bufText.Split(' ').ToList<string>();

            for (int i = originWords.Count - 1; i >= 0; i--)
            {
                originWords[i] = originWords[i].Trim();
                if (originWords[i] == "")
                    originWords.RemoveAt(i);

                progressBar.Value = (int)(progressBar.Maximum *0.6 + progressBar.Maximum *0.01 * (originWords.Count - i) / originWords.Count);
            }

            allWordsCount = originWords.Count;
            lbAllWords.Text = "Всего слов  -  " + allWordsCount;
            lbAllWords.Update();

            List<string> uniqueWords = new List<string>();
            Dictionary<string, int> words = new Dictionary<string, int>();

            for (int i = 0; i < originWords.Count; i++)
            {
                if (!uniqueWords.Contains(originWords[i]))//если такого слова ещё не встречалось
                {
                    uniqueWords.Add(originWords[i]);
                    words.Add(originWords[i], 1);
                }
                else
                {
                    words[originWords[i]]++;
                }

                uniqWordsStatistics.Add(i, (double)uniqueWords.Count / (i+1));//доля уникальных слов при достижении данного слова

                progressBar.Value = (int)(progressBar.Maximum *0.61 + progressBar.Maximum *0.12 * i / originWords.Count);
            }

            lbUniqueWords.Text = "Уникальных слов  -  " + uniqueWords.Count
                + " (" + Math.Round(100.00 * uniqueWords.Count / allWordsCount, 2) + "%)";
            uniqWordsCount = uniqueWords.Count;
            lbUniqueWords.Update();

            lbAvergWordsLen.Text = "Средняя длина слова  -  " + Math.Round(allWordCharsCount / allWordsCount, 2);
            lbAvergWordsLen.Update();

            lbAvergWordsInParagraph.Text = "В среднем слов в абзаце  -  " + Math.Round(allWordsCount / paragraphsCount, 2);
            lbAvergWordsInParagraph.Update();

            lbAvergCharsInParagraph.Text = "В среднем символов в абзаце  -  " + Math.Round(allCharsCount / paragraphsCount, 2);
            lbAvergCharsInParagraph.Update();

            uniqueWords.Sort();
            words = words.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);//сортировка словаря по значениям по убыванию

            UniqWordsStatisticGraphicCreate();//построение графика статистики уникальности слов


            int uniqWordsNumLen = uniqWordsCount.ToString().Length;

            SortedDictionary<string, int> origins = new SortedDictionary<string, int>();

            //////////
            string outputWords = "";

            rtbWords.Text = "";
            for (int i = 0; i < uniqueWords.Count; i++)
            {
                outputWords += uniqueWords[i] + "  "+ words[uniqueWords[i]] + "("
                    + Math.Round(100.00 * words[uniqueWords[i]] / uniqWordsCount, uniqWordsNumLen - 1) + "%)\n";

                if (origins.ContainsKey(uniqueWords[i].Substring(0, 1)))
                {
                    origins[uniqueWords[i].Substring(0, 1)]++;
                }
                else
                    origins.Add(uniqueWords[i].Substring(0, 1), 1);

                progressBar.Value = progressBar.Value = (int)(progressBar.Maximum *0.73 + progressBar.Maximum *0.11 * i / uniqueWords.Count);
            }

            rtbWords.Text = outputWords;

            /////////////
            string popularWords = "";

            int n = 1;

            rtbPopularWords.Text = "";
            foreach(string s in words.Keys)
            {
                for(int l=0; l< uniqWordsNumLen- n.ToString().Length; l++)
                {
                    popularWords += "0";
                }
                popularWords += n+". "+s + "  " + words[s] + "("
                    + Math.Round(100.00 * words[s] / uniqWordsCount, uniqWordsNumLen - 1) + "%)\n";

                progressBar.Value = (int)(progressBar.Maximum * 0.84 + progressBar.Maximum *0.15 * n / uniqueWords.Count);
                n++;
            }

            rtbPopularWords.Text = popularWords;

            ///////////
            string wordOrigins = "";

            int originCountTextLen = origins.Count.ToString().Length;

            n = 1;
            foreach (string s in origins.Keys)
            {
                wordOrigins += s + "  " + origins[s] + "("
                    + Math.Round(100.00 * origins[s] / uniqWordsCount, originCountTextLen) + "%)\n";

                progressBar.Value = (int)(progressBar.Maximum * 0.99 + progressBar.Maximum *0.01 * n / uniqueWords.Count);
                n++;
            }

            rtbOrigins.Text = wordOrigins;


            progressBar.Value = progressBar.Maximum;
            progressBar.ForeColor = Color.Blue;

            MessageBox.Show("Анализ закончен!");
        }


        /// <summary>
        /// Функция построения графика статистики уникальности слов при последовательном их чтении
        /// </summary>
        public void UniqWordsStatisticGraphicCreate()
        {
            if (uniqWordsStatistics.Count > 0)
            {
                Graphics gr = panelUniqWordsStatisticGraphic.CreateGraphics();

                Double[] points = new double[panelUniqWordsStatisticGraphic.Width];

                for (int i = 0; i < panelUniqWordsStatisticGraphic.Width; i++)
                {
                    double approxKey = (uniqWordsStatistics.Count - 1) / panelUniqWordsStatisticGraphic.Width * i;
                    points[i] = panelUniqWordsStatisticGraphic.Height * uniqWordsStatistics[(int)Math.Round(approxKey)];
                }

                Pen p = new Pen(Color.Blue, 1);// цвет линии и ширина
                for (int i = 1; i < panelUniqWordsStatisticGraphic.Width; i++)
                {
                    gr.DrawLine(p, i - 1, panelUniqWordsStatisticGraphic.Height - (int)points[i - 1], i, panelUniqWordsStatisticGraphic.Height - (int)points[i]);// рисуем линию
                }
            }

        }

        private void TextAnalizer_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button==MouseButtons.Left)
            {
                mouseDown = true;
            }
        }

        private void TextAnalizer_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseDown = false;
            }
        }

        private void TextAnalizer_Resize(object sender, EventArgs e)
        {
            if(!mouseDown)//если форма "отпущена"
            {
                Graphics gr = panelUniqWordsStatisticGraphic.CreateGraphics();
                gr.Clear(panelUniqWordsStatisticGraphic.BackColor);

                UniqWordsStatisticGraphicCreate();
            }
        }
    }
}
